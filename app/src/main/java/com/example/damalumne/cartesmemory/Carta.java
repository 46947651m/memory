package com.example.damalumne.cartesmemory;

public class Carta {
    private int frontImage;
    private int backImage;
    private Estat estat;

    public Carta(int backImage, int frontImage){
        this.backImage = backImage;
        this.frontImage = frontImage;
        this.estat = Estat.BACK;
    }

    public int getImagen(){
        if(this.estat == Estat.FRONT || this.estat == Estat.FIXED){
            return frontImage;
        }else{
            return backImage;
        }
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }

    public void girar(){
        if(this.estat == Estat.BACK){
            this.estat = Estat.FRONT;
        }else if(this.estat == Estat.FRONT){
            this.estat = Estat.BACK;
        }
    }

    enum Estat {
        FIXED,
        FRONT,
        BACK;
    }
}
