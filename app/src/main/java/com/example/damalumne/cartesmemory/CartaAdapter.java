package com.example.damalumne.cartesmemory;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.CartaViewHolder>{
    private List<Carta> items;
    private Context context;
    private boolean activo = true;
    private List<Carta> pareja = new ArrayList();
    final Handler handler = new Handler();

    public  class CartaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imagen;

        public CartaViewHolder(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            imagen.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (activo) {

                Carta c = items.get(getAdapterPosition());
                if (pareja.size() == 1 && c == pareja.get(0)) {

                } else {
                    c.girar();
                    notifyDataSetChanged();


                    pareja.add(c);
                    if (pareja.size() == 2) {
                        activo = false;
                        if (pareja.get(0).getImagen() != pareja.get(1).getImagen()) {

                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    pareja.get(0).girar();
                                    pareja.get(1).girar();
                                    pareja.clear();
                                    notifyDataSetChanged();
                                    activo = true;
                                }
                            }, 600);


                        } else {
                            pareja.get(0).setEstat(Carta.Estat.FIXED);
                            pareja.get(1).setEstat(Carta.Estat.FIXED);
                            notifyDataSetChanged();
                            pareja.clear();
                            activo = true;
                        }

                    }
                }
            }
        }
    }

    public CartaAdapter(List items, Context context){
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public CartaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.carta, viewGroup, false);
        return new CartaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CartaViewHolder viewHolder, int i) {
        viewHolder.imagen.setImageResource(items.get(i).getImagen());
    }
}
