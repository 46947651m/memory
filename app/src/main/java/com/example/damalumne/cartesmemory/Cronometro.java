package com.example.damalumne.cartesmemory;

import android.app.Activity;
import android.os.CountDownTimer;
import android.widget.TextView;

public class Cronometro extends CountDownTimer {

    private TextView crono;

    public Cronometro(long millisInFuture, long countDownInterval, TextView crono) {
        super(millisInFuture, countDownInterval);
        this.crono = crono;
    }

    @Override
    public void onTick(long l) {
        crono.setText("seconds remaining: " + l / 1000);
    }

    @Override
    public void onFinish() {
        crono.setText("Fin");
    }
}
