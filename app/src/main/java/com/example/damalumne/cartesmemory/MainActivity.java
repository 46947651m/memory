package com.example.damalumne.cartesmemory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Chronometer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView crono = (TextView) findViewById(R.id.cronometro);
        Cronometro cr = new Cronometro(30000, 1000, crono);
        cr.start();

        List<Carta> disponibles = new ArrayList();
        disponibles.add(new Carta(R.drawable.back, R.drawable.c0));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c0));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c1));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c1));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c2));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c2));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c3));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c3));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c4));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c4));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c5));
        disponibles.add(new Carta(R.drawable.back, R.drawable.c5));

        List<Carta> cartas = new ArrayList<>();

        while(disponibles.size() > 0) {
            int random = new Random().nextInt(disponibles.size());
            Carta c = disponibles.get(random);
            cartas.add(c);
            disponibles.remove(c);
        }

        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        lManager = new GridLayoutManager(this, 3);
        recycler.setLayoutManager(lManager);

        adapter = new CartaAdapter(cartas, this);
        recycler.setAdapter(adapter);
    }
}